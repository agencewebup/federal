<?php

namespace federal;

use \Exception;

require_once 'Mysql.php';

class Federal
{
    private $ignoredFiles = array('.', '..', 'empty', '.DS_Store');

    public function main($options)
    {
        try {
            $options = $this->checkOptions($options);

            if ($options['command'] == 'update') {
                $this->update($options);

            } elseif ($options['command'] == 'apply') {
                $this->apply($options);

            } elseif ($options['command'] == 'clear') {
                $this->clear($options);
            }

        } catch (Exception $e) {
            echo "\n\nError: \n" . $e->getMessage() . "\n";
        }
    }

    public function help()
    {
        echo <<<EOT
usage: federal.php <command>

Liste des commandes
    update      Import et update les dépendances du projet


EOT;
    }

    public function checkOptions($options)
    {
        $opts = array();

        if (isset($options['host'])) {
            $opts['host'] = $options['host'];
        } else {
            throw new Exception("Option --host required", 1);
        }

        if (isset($options['port'])) {
            $opts['port'] = $options['port'];
        } else {
            throw new Exception("Option --port required", 1);
        }

        if (isset($options['database'])) {
            $opts['database'] = $options['database'];
        } else {
            throw new Exception("Option --database required", 1);
        }

        if (isset($options['user'])) {
            $opts['user'] = $options['user'];
        } else {
            throw new Exception("Option --user required", 1);
        }

        if (isset($options['password'])) {
            $opts['password'] = $options['password'];
        } else {
            $opts['password'] = '';
        }

        if (isset($options['update'])) {
            $opts['command'] = 'update';
            $opts['directory'] = $options['update'];
            if (isset($options['scripts'])) {
                $opts['scripts'] = $options['scripts'];
            }
        } elseif (isset($options['apply'])) {
            $opts['command'] = 'apply';
            $opts['file'] = $options['apply'];
        } elseif (isset($options['clear'])) {
            $opts['command'] = 'clear';
        } else {
            throw new Exception("Write help :)", 1);
        }

        return $opts;
    }

    public function apply($opts)
    {
        if (! isset($opts['file']) || ! is_file($opts['file'])) {
            throw new Exception("Fichier '".$opts['file']."' introuvable", 1);
        }

        $db = new Mysql();
        $db->connect($opts['host'], $opts['port'], $opts['database'], $opts['user'], $opts['password']);
        $this->applyPatch($db, $opts['file']);
    }

    public function clear($opts)
    {
        $db = new Mysql();
        $db->connect($opts['host'], $opts['port'], $opts['database'], $opts['user'], $opts['password']);
        $this->clearDb($db);
    }

    public function update($opts)
    {
        if (! isset($opts['directory']) || ! is_dir($opts['directory'])) {
            throw new Exception("Répertoire '".$opts['directory']."' des patchs introuvable", 1);
        }

        $directory = $opts['directory'];
        $db = new Mysql();
        $db->connect($opts['host'], $opts['port'], $opts['database'], $opts['user'], $opts['password']);
        $this->createVersionTable($db);
        $lastPatch = $this->getVersion($db);

        foreach (scandir($directory) as $file) {
            if (! in_array($file, $this->ignoredFiles) && $file > $lastPatch) {
                $this->applyPatch($db, $directory . DIRECTORY_SEPARATOR . $file);

                if (isset($opts['scripts'])) {
                    $script = $opts['scripts'] . DIRECTORY_SEPARATOR . basename($file, '.sql').'.php';
                    if (file_exists($script)) {
                        $this->applyScript($script);
                    }
                }

                $this->saveVersion($db, $file);
            }
        }
    }

    private function applyPatch($db, $filename)
    {
        echo "Apply: " . $filename;

        $db->executeFile($filename);

        echo " \x1B[32mOK\x1B[0m\n";
    }

    private function applyScript($filename)
    {
        echo "Apply script: " . $filename;
        exec("php $filename", $output, $return_var);

        if ($return_var) {
            exit(1);
        } else {
            echo " \x1B[32mOK\x1B[0m\n";
        }
    }

    private function clearDb($db)
    {
        foreach ($db->execute('SHOW TABLES') as $row) {
            $db->execute('DROP TABLE IF EXISTS `'.$row[0].'`');
        }
    }

    private function createVersionTable($db)
    {
        $exists = $db->execute("SHOW TABLES LIKE '__federal'")->rowCount();

        if (!$exists) {
            $db->execute("CREATE TABLE `__federal` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `key` varchar(16) DEFAULT NULL, `value` varchar(64) DEFAULT NULL, PRIMARY KEY (`id`))");
            $db->execute("INSERT INTO `__federal` (`key`,`value`) VALUES('VERSION','')");
        }
    }

    private function saveVersion($db, $version)
    {
        $db->execute("UPDATE `__federal` SET value=? WHERE `key`='version'", array($version));
    }

    private function getVersion($db)
    {
        return $db->execute("SELECT * FROM `__federal` WHERE `key`='VERSION'")->fetch(\PDO::FETCH_ASSOC)['value'];
    }
}
