<?php

namespace federal;

class Mysql
{
    private $db;

    public function connect($host, $port, $database, $user, $password)
    {
        $dsn = 'mysql:host=' . $host . ';port=' . $port . ';dbname=' . $database;
        $setUtf8 = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

        $this->db = new \PDO($dsn, $user, $password, $setUtf8);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function disconnect()
    {
        $this->db = null;
    }

    public function describe($table)
    {
        $sql = sprintf('DESCRIBE `%s`', $table);
        $statement = $this->execute($sql);

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Execute une requete
     *
     * @param  string $sql      Requete SQL
     * @param  array  $bindings Valeur à binder de la requete
     * @return PDOStatement
     */
    public function execute($sql, $bindings = array())
    {
        $statement = $this->db->prepare($sql);

        foreach ($bindings as $key => $value) {
            if (is_int($key)) {
                ++$key;
            } else {
                $key = ':' . $key;
            }
            $statement->bindValue($key, $value);
        }

        $statement->execute();

        return $statement;
    }

    /**
     * Execute un fichier sql
     * @param  string $filename chemin du fichier
     */
    public function executeFile($filename)
    {
        if (! file_exists($filename)) {
            throw new \Exception("Patch ". $filename . "not found", 1);
        }

        $sql = file_get_contents($filename);
        $stmt = $this->execute($sql);
        while ($stmt->nextRowset()) {
        }
    }
}
