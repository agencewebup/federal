<?php

require_once "src/Federal.php";

$shortopts  = "";

$longopts  = array(
    // config
    "host:",
    "port:",
    "database:",
    "user:",
    "password:",
    // commande
    "apply:",
    "update:",
    "scripts:",
    "clear",
);

$options = getopt($shortopts, $longopts);

$federal = new \federal\Federal();
$federal->main($options);
