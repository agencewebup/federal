# federal

## Commandes

--apply=file          Applique un patch
--update=directory    Applique tous les patch d'un repertoire à partir du dernier appliquer
--clear               Supprimer toutes les tables de la db

## Options obligatoire

--host=hostname
--port=port
--database=db_name
--user=user
--password=pwd

## Exemple de commande

$ federal.php --host 192.168.50.4 --port 5017 --database db --user root --password="" --clear
$ federal.php --host 192.168.50.4 --port 5017 --database db --user root --password="" --apply="db/patch/01-structure.sql"
$ federal.php --host 192.168.50.4 --port 5017 --database db --user root --password="" --update="db/patch"